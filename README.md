# Agenda

## Installation

(For english spoken, please use Google trad :P)

```git clone https://gitlab.com/DavidC212/agenda-from-uml.git```

```./vendor/bin/sail up -d ```

possibilité de créer un alias dans le .bash_aliases :

    alias sail='sh $([ -f sail ] && echo sail || echo vendor/bin/sail)'

```php artisan migrate```

## Diagramme de classes

(Hum... P'tits problèmes de conversion d'image, donc faudra imaginer certains traits...)

![Diagramme de classe](agenda_uml.jpg "Diagramme de classe")
