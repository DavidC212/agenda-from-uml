<?php

use App\Http\Controllers\AgendaController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';

Route::prefix('contact')->group(function () {
    Route::get('/create', [ContactController::class, 'create'])->name('create');
    Route::get('/store', [ContactController::class, 'store'])->name('store');
    Route::get('/edit', [ContactController::class, 'edit'])->name('edit');
   Route::get('/update', [ContactController::class, 'update'])->name('update');
   Route::get('/destroy', [ContactController::class, 'destroy'])->name('destroy');
});



