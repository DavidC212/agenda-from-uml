<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Coucou') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 dark:text-gray-100">
                    <div>
                        <h1 style="margin:2%">New Contact</h1>
                        <div class="row">
                            <div class="column large-12">

                                <div class="row row-x-center">
                                    <div class="column large-6 tab-12">
                                        <form class="s-content__form" method="post" action="{{route('store')}}">
                                            @csrf
                                            <fieldset>
                                                <div style="margin:2%;">
                                                    <label for="name">Name</label>
                                                    <input id="name" class="h-full-width" type="text" name="name" placeholder="Name" required autofocus>
                                                </div>
                                                <div style="margin:2%;">
                                                    <label for="email">Email</label>
                                                    <input id="email" class="h-full-width" type="email" name="email" placeholder="Email" required autofocus>
                                                </div>
                                                <div style="margin:2%;">
                                                    <label for="phone">Phone</label>
                                                    <input id="phone" class="h-full-width" type="text" name="phone" placeholder="Phone" required autofocus>
                                                </div>
                                                <div style="margin:2%;">
                                                    <label for="Address">Address</label>
                                                    <input id="address" class="h-full-width" type="text" name="adress" placeholder="Address" required autofocus>
                                                </div>
                                                <div style="margin:2%;">
                                                    <label for="website">Website</label>
                                                    <input id="website" class="h-full-width" type="text" name="website" placeholder="Website" required autofocus>
                                                </div>

                                                <button style="margin:2%">Create</button>
                                            </fieldset>
                                        </form>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
