<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Email extends ContactDetail
{

    private string $regex_email = "/^[\w-.]+@([\w-]+.)+[\w-]{2,4}$/";
    private string $email;

    public function validate(string $regex_email, string $email)
    {
        return parent::validate($regex_email, $email);

//       Validator::extend('validate_email_format', function($attribute, $value, $parameters, $validator){
//           return filter_var($value, FILTER_VALIDATE_EMAIL) !== false;
//       });
    }

//    public static $emailValidationRules = [
//        'email' => 'required|email|validate_email_format'
//    ];
//
//    public static function validateEmail($data){
//        return Validator::make($data, static::$emailValidationRules);
//    }
}
