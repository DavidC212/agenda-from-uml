<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Adress extends ContactDetail
{
    public string $regex_address="/([0-9]) ?([a-zA-Z,. ]) ?([0-9]{5}) ?([a-zA-Z]*)/";
    public string $address_name;

    public function validate(string $regex_address, string $address_name)
    {
        return parent::validate($regex_address, $address_name);
    }
}
