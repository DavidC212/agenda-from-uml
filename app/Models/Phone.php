<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Phone extends ContactDetail
{

    private string $regex_phone = '/^[+]?[(]?[0-9]{3}[)]?[-\s.]?[0-9]{3}[-\s.]?[0-9]{4,6}$/';
    private string $phone;

    public function validate(string $regex_phone, string $phone)
    {
        return parent::validate($regex_phone, $phone);
    }
}
