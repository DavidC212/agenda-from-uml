<?php

namespace App\Models;

use http\Exception;
use Throwable;

abstract class ContactDetail
{

    public function validate(string $regex, string $value)
    {
        if (preg_match($regex, $value) === 1) {
            return true;
        } else {
            return false;
        }
    }
}
