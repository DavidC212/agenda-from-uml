<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Website extends ContactDetail
{

    public string $regex_www="/(https://www.|http://www.|https://|http://)?[a-zA-Z0-9]{2,}(.[a-zA-Z0-9]{2,})(.[a-zA-Z0-9]{2,})?/";
    public string $www_name;

    public function validate(string $regex_www, string $www_name)
    {
        return parent::validate($regex_www, $www_name);
    }
}
